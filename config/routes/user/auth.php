<?php
declare(strict_types=1);

return [
    [
        'method'      =>  'GET',
        'pattern'     =>  'v1/user/auth',
        'controller'  =>  \web\v1\user\AuthController::class,
        'action'      =>  'login'
    ],
    [
        'method'      =>  'POST',
        'pattern'     =>  'v1/user/auth',
        'controller'  =>  \web\v1\user\AuthController::class,
        'action'      =>  'registration'
    ],
    [
        'method'      =>  'PUT',
        'pattern'     =>  'v1/user/auth',
        'controller'  =>  \web\v1\user\AuthController::class,
        'action'      =>  'login'
    ],
    [
        'method'      =>  'DELETE',
        'pattern'     =>  'v1/user/auth',
        'controller'  =>  \web\v1\user\AuthController::class,
        'action'      =>  'logout'
    ],



];