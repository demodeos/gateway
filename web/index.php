<?php
define("APP", __DIR__.'/../');

require_once "function.php";
require_once __DIR__."/../vendor/autoload.php";


$config = require_once __DIR__.'/../config/config.php';

use Demodeos\Api\Core;


Core::init($config);