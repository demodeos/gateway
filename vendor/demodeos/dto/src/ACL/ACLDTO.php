<?php
declare(strict_types=1);

namespace Demodeos\Dto\ACL;

use Demodeos\Dto\AbstractDTO;

class ACLDTO extends AbstractDTO
{

    public int|string   $id;
    public string       $method = '';
    public string       $version = '';
    public string       $module = '';
    public string       $controller = '';
    public string       $action = '';
    public string       $url = '';
    public string       $type = 'auto';
    public string       $description = 'auto';
    public int|string   $role = 1;

}