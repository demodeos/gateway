<?php
declare(strict_types=1);

namespace Demodeos\Dto;

use ReflectionClass;
use ReflectionProperty;

class AbstractDTO
{



    public function load(array|object $data): static
    {
        $reflection = new ReflectionClass($this);

        foreach ($reflection->getProperties(ReflectionProperty::IS_PUBLIC) as $property)
            if(isset($data[$property->name]))
                $this->{$property->name} = $data[$property->name];

        return $this;
    }

}